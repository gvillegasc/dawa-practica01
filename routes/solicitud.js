var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('solicitud', { title: 'Solicitud' });
});

router.post('/', function (req, res){
	res.send('Solicitud Realizada!!')
});

module.exports = router;